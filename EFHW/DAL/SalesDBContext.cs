﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace EFHW.DAL
{
    
    class MyContextInitializer : CreateDatabaseIfNotExists<SalesDBContext>
    {
        protected override void Seed(SalesDBContext db)
        {
            var products = new List<DAL.Product>()
            {
                new DAL.Product { Name = "Product1", Price=10},
                new DAL.Product { Name = "Product2", Price=12.5},
                new DAL.Product { Name = "Product3", Price=15},
                new DAL.Product { Name = "Product4", Price=17.5},
                new DAL.Product { Name = "Product5", Price=20},
            };

            db.Products.AddOrUpdate(x => new { x.Name, x.Price }, products.ToArray());
            db.SaveChanges();

            var orders = new List<DAL.Order>()
            {
                new DAL.Order { Date = new DateTime(2019, 10, 22), Qty = 2, OrderNo = 1, ProductId = products[0].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 22), Qty = 3, OrderNo = 1, ProductId = products[1].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 22), Qty = 4, OrderNo = 1, ProductId = products[2].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 26), Qty = 1, OrderNo = 2, ProductId = products[3].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 26), Qty = 2, OrderNo = 2, ProductId = products[4].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 29), Qty = 1, OrderNo = 3, ProductId = products[3].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 29), Qty = 1, OrderNo = 3, ProductId = products[2].Id }

            };

            db.Orders.AddOrUpdate(o => new { o.Date, o.Qty, o.OrderNo, o.ProductId }, orders.ToArray());
            db.SaveChanges();
        }
    }

    class SalesDBContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }

        static SalesDBContext()
        {
            Database.SetInitializer<SalesDBContext>(new MyContextInitializer());
        }

        public SalesDBContext() : base("SalesDBConnection")
        {
          
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace EFHW.Model
{
    class OrderDetailItem : PropertyChangedBase
    {
        public int Id { get; set; }
        private DAL.Product product = new DAL.Product();
        [DependentProperties("Sum")]
        public DAL.Product Product { get => product; set => SetField(ref product, value); } 

        private int qty;
        [DependentProperties("Sum")]
        public int Qty { get => qty; set => SetField(ref qty, value); }
        public double Sum => Product.Price * Qty;

    }
}

﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using EFHW.Model;

namespace EFHW
{
    public enum EditMode
    {
        View,
        Edit,
        New
    }
    /// <summary>
    /// Логика взаимодействия для OrderDetails.xaml
    /// </summary>
    public partial class OrderDetails : Window
    { 
        public OrderDetails()
        {
            InitializeComponent();
        }

    }
}

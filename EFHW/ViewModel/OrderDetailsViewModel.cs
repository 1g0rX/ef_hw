﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Windows;
using EFHW.Model;
using Utils;

namespace EFHW.ViewModel
{
    class OrderDetailsViewModel : Utils.PropertyChangedBase
    {
        #region Properties
        private bool isViewMode = false;
        public bool IsViewMode
        {
            get { return isViewMode; }
            set { SetField(ref isViewMode, value); }
        }

        private bool isAddMode = false;
        public bool IsAddMode
        {
            get { return isAddMode; }
            set { SetField(ref isAddMode, value); }
        }

        private int orderNo;
        public int OrderNo
        {
            get { return orderNo; }
            set { SetField(ref orderNo, value); }
        }

        private DateTime documentDate;
        public DateTime DocumentDate
        {
            get { return documentDate; }
            set { SetField(ref documentDate, value); }
        }

        private ObservableCollection<DAL.Product> products = new ObservableCollection<DAL.Product>();
        public ObservableCollection<DAL.Product> Products
        {
            get { return products; }
            set { SetField(ref products, value); }
        }


        private ObservableCollection<OrderDetailItem> orderDetails = new ObservableCollection<OrderDetailItem>();
        public ObservableCollection<OrderDetailItem> OrderDetails
        {
            get { return orderDetails; }
            set { SetField(ref orderDetails, value); }
        }

        private OrderDetailItem selectedOrder = new OrderDetailItem();
        public OrderDetailItem SelectedOrder
        {
            get { return selectedOrder; }
            set { SetField(ref selectedOrder, value); }
        }
        #endregion

        public OrderDetailsViewModel(EditMode mode, int orderNo = 0)
        {
            using (DAL.SalesDBContext context = new DAL.SalesDBContext())
            {
                foreach (DAL.Product product in context.Products)
                {
                    Products.Add(product);
                }

                if (mode == EditMode.New)
                {
                    IsAddMode = true;
                    OrderNo = context.Orders.Count() == 0 ? 1 : (context.Orders.Max(o => o.OrderNo) + 1);
                    DocumentDate = DateTime.Today;
                }

                if (mode == EditMode.View | mode == EditMode.Edit)
                {
                    if (mode == EditMode.View) IsViewMode = true;
                    var orders = context.Orders.Where(o => o.OrderNo == orderNo).Include(p => p.Product);
                    DocumentDate = orders.FirstOrDefault().Date;
                    OrderNo = orders.FirstOrDefault().OrderNo;

                    foreach (var item in orders)
                    {
                        OrderDetails.Add(new OrderDetailItem { Id = item.Id, Product = item.Product, Qty = item.Qty });
                    }

                }
            }
        }

        public RelayCommand updateDB;
        public RelayCommand UpdateDB
        {
            get
            {
                return updateDB ??
                  (updateDB = new RelayCommand(obj =>
                  {
                      using (DAL.SalesDBContext context = new DAL.SalesDBContext())
                      {
                          List<DAL.Order> orders = new List<DAL.Order>();

                          foreach (var item in OrderDetails)
                          {
                              orders.Add(new DAL.Order { OrderNo = this.OrderNo, Date = this.DocumentDate, ProductId = item.Product.Id, Qty = item.Qty });
                          }
                             
                          context.Orders.AddOrUpdate(o => new { o.OrderNo, o.Date, o.ProductId, o.Qty}, orders.ToArray());
                          context.SaveChanges();
                      }
                  }));
            }
        }

        private RelayCommand deleteOrder;
        public RelayCommand DeleteOrder
        {
            get
            {
                return deleteOrder ??
                  (deleteOrder = new RelayCommand(obj =>
                  {
                      using (DAL.SalesDBContext context = new DAL.SalesDBContext())
                      {
                          DAL.Order order = context.Orders.Find(SelectedOrder.Id);

                          if (order != null)
                          {
                              context.Orders.Remove(order);
                              OrderDetails.Remove(SelectedOrder);
                          }
                          context.SaveChanges();
                      }
                  }));
            }
        }
    }
}

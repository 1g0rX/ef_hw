﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFHW.Model
{
    class OrderItem
    {
        public int OrderNo { get; set; }
        public DateTime Date { get; set; }
        public double Sum { get; set; }
    }
}

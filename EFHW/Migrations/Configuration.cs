﻿namespace EFHW.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EFHW.DAL.SalesDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EFHW.DAL.SalesDBContext context)
        {
            var products = new List<DAL.Product>()
            {
                new DAL.Product { Name = "Product1", Price=100.99},
                new DAL.Product { Name = "Product2", Price=200.99},
                new DAL.Product { Name = "Product3", Price=300.99},
                new DAL.Product { Name = "Product4", Price=400.99},
                new DAL.Product { Name = "Product5", Price=500.99},
            };

            context.Products.AddOrUpdate(x => new { x.Name, x.Price }, products.ToArray());
            context.SaveChanges();

            var orders = new List<DAL.Order>()
            {
                new DAL.Order { Date = new DateTime(2019, 10, 22), Qty = 10, OrderNo = 1, ProductId = products[0].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 22), Qty = 15, OrderNo = 1, ProductId = products[1].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 22), Qty = 2, OrderNo = 1, ProductId = products[2].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 26), Qty = 1, OrderNo = 2, ProductId = products[3].Id },
                new DAL.Order { Date = new DateTime(2019, 10, 26), Qty = 2, OrderNo = 2, ProductId = products[4].Id }

            };

            context.Orders.AddOrUpdate(o => new { o.Date, o.Qty, o.OrderNo, o.ProductId }, orders.ToArray());
            context.SaveChanges();
        }
    }
}

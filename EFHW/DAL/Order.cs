﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Utils;

namespace EFHW.DAL
{
    class Order : PropertyChangedBase
    {
        public int Id { get; set; }
        public int OrderNo { get; set; }
        public DateTime Date { get; set; }
        public int? ProductId { get; set; }
        public int Qty { get; set; }
        [NotMapped]
        public double Sum => Product?.Price == null ? 0 : Product.Price * Qty;
        
        public Product Product { get; set; }

        public Order()
        {
            //Products = new HashSet<Product>();
        }
    }
}

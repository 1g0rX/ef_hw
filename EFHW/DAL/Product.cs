﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace EFHW.DAL
{
    class Product
    {
        public int Id { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        public Double Price { get; set; }

        public ICollection<Order> Orders { get; set; }

        public Product()
        {
            Orders = new HashSet<Order>();
        }
    }
}

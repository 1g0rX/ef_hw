﻿using EFHW.Model;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using Utils;

namespace EFHW.ViewModel
{
    class MainViewModel : Utils.PropertyChangedBase
    {
        private ObservableCollection<OrderItem> orders = new ObservableCollection<OrderItem>();
        public ObservableCollection<OrderItem> Orders
        {
            get => orders;
            set => SetField(ref orders, value);
        }

        private OrderItem selectedOrder;

        public OrderItem SelectedOrder
        {
            get { return selectedOrder; }
            set { SetField(ref selectedOrder, value); }  
        }

        private int selectedIndex;
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set { SetField(ref selectedIndex, value); }
        }


        public MainViewModel()
        {
            FillOrders();
            selectedIndex = 0;
        }

        private void FillOrders()
        {
            Orders.Clear();

            using (DAL.SalesDBContext context = new DAL.SalesDBContext())
            {
                var result = (from o in context.Orders.Include(o => o.Product)
                              group o by new { o.OrderNo, o.Date }
                             into grp
                              select new Model.OrderItem
                              {
                                  OrderNo = grp.Key.OrderNo,
                                  Date = grp.Key.Date,
                                  Sum = grp.Sum(t => t.Qty * t.Product.Price)
                              }).OrderBy(d => d.Date);


                foreach (var item in result)
                {
                    Orders.Add(item);
                }
            }

        }

        public RelayCommand addOrder;
        public RelayCommand AddOrder
        {
            get
            {
                return addOrder ??
                  (addOrder = new RelayCommand(obj =>
                  {
                      OrderDetails orderDetails = new OrderDetails();
                      orderDetails.DataContext = new ViewModel.OrderDetailsViewModel(EditMode.New);
                      orderDetails.ShowDialog();

                      FillOrders();
                  }));
            }
        }

        public RelayCommand editOrder;
        public RelayCommand EditOrder
        {
            get
            {
                return editOrder ??
                  (editOrder = new RelayCommand(obj =>
                  {
                      if (selectedOrder == null) return;

                      OrderDetails orderDetails = new OrderDetails();
                      orderDetails.DataContext = new ViewModel.OrderDetailsViewModel(EditMode.Edit, SelectedOrder.OrderNo);
                      orderDetails.ShowDialog();

                      int index = SelectedIndex;
                      FillOrders();
                      SelectedIndex = index;
                  }));
            }
        }

        public RelayCommand showOrder;
        public RelayCommand ShowOrder
        {
            get
            {
                return showOrder ??
                  (showOrder = new RelayCommand(obj =>
                  {
                      if (selectedOrder == null) return;

                      OrderDetails orderDetails = new OrderDetails();
                      orderDetails.DataContext = new ViewModel.OrderDetailsViewModel(EditMode.View, SelectedOrder.OrderNo);
                      orderDetails.ShowDialog();
                  }));
            }
        }

        public RelayCommand deleteOrder;
        public RelayCommand DeleteOrder
        {
            get
            {
                return deleteOrder ??
                  (deleteOrder = new RelayCommand(obj =>
                  {
                      if (selectedOrder == null) return;
                      using (DAL.SalesDBContext context = new DAL.SalesDBContext())
                      {


                          var orders = context.Orders.Where(o => o.OrderNo == SelectedOrder.OrderNo);
                          context.Orders.RemoveRange(orders);
                          context.SaveChanges();

                          Orders.Remove(SelectedOrder);
                        
                      }
                  }));
            }
        }
    }
}
